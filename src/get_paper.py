import argparse
import os
import pandas as pd
import requests
import xml.etree.ElementTree as et
from bs4 import BeautifulSoup
from time import sleep

class NBER:
    
    def __init__(self):
        self.id = args.initial_id
        self.timeout = args.timeout
        self.sleep = args.sleep

    def get_latest_paper(self):
        url = 'https://data.nber.org/new.html#latest'
        response = requests.get(url)
        content = BeautifulSoup(response.content, features='html.parser')
        latest = content.find('li', {'class': 'multiline-li'})
        latest = latest.find('a').attrs['href']
        latest = int(latest.replace('https://data.nber.org/papers/w', ''))

        return latest

    def get_citation_item(self, content, item):
        item = content.find('meta', {'name': f'{item}'})
        try:
            item = item.attrs['content']
        except AttributeError:
            item = None

        return item

    def get_citation_author(self, content):
        author = content.find_all('meta', {'name': 'citation_author'})
        author = [x.get('content') for x in author]

        return author

    def get_topics(self, content):
        try:
            bibtop = content.find('p', {'class': 'bibtop'})
            topics = bibtop.find_all('a')
            topics = [x.get_text() for x in topics]
        except AttributeError:
            topics = None

        return topics

    def get_abstract(self, content):
        try:
            abstract = content.find('p', {'style': 'margin-left: 40px; margin-right: 40px; text-align: justify'})
            abstract = abstract.contents[0].replace('\n', '')
            if '\x00' in abstract:
                abstract = abstract.replace('\x00', '')
        except AttributeError:
            abstract = None

        return abstract

    def get_also_downloaded(self, content):
        try:
            also_downloaded = content.find('table', {'class': 'also-downloaded'})
            also_downloaded = also_downloaded.find_all('td')
            also_downloaded = [x.find('a') for x in also_downloaded if x.find('a') != None]
            also_downloaded = [x.attrs['href'] for x in also_downloaded]
        except AttributeError:
            also_downloaded = None

        return also_downloaded

    def get_total_cites(self, nber_id):
        if 0 <= nber_id < 10:
            nber_id = f'000{nber_id}'
        elif 10 <= nber_id < 100:
            nber_id = f'00{nber_id}'
        elif 100 <= nber_id < 1000:
            nber_id = f'0{nber_id}'
        status_code = None
        while status_code != 200:
            try:
                url = f'http://citec.repec.org/api/plain/RePEc:nbr:nberwo:{nber_id}'
                response = requests.get(url, timeout=self.timeout)
                status_code = response.status_code
                xml = et.fromstring(response.text)
                total_cites = int(xml.find('cites').text)
            except Exception:
                print(f'Exception happens to get total cited by in WP {nber_id}. Don\'t worry, we\'ll try again.')
                sleep(self.sleep)
                continue

        try:
            return total_cites
        except UnboundLocalError:
            return None

    def get_cited_by(self, nber_id):
        if 0 <= nber_id < 10:
            nber_id = f'000{nber_id}'
        elif 10 <= nber_id < 100:
            nber_id = f'00{nber_id}'
        elif 100 <= nber_id < 1000:
            nber_id = f'0{nber_id}'
        status_code = None
        while status_code != 200:
            try:
                url = f'http://citec.repec.org/api/plain/RePEc:nbr:nberwo:{nber_id}'
                response = requests.get(url, timeout=self.timeout)
                status_code = response.status_code
                xml = et.fromstring(response.text)
                cited_by = int(xml.find('citedBy').text)
            except Exception:
                print(f'Exception happens to get total cited by in WP {nber_id}. Don\'t worry, we\'ll try again.')
                sleep(self.sleep)
                continue

        try:
            return cited_by
        except UnboundLocalError:
            return  None

    def get_reference(self, nber_id):
        if 0 <= nber_id < 10:
            nber_id = f'000{nber_id}'
        elif 10 <= nber_id < 100:
            nber_id = f'00{nber_id}'
        elif 100 <= nber_id < 1000:
            nber_id = f'0{nber_id}'
        status_code = None
        while status_code != 200:
            try:
                url = f'http://citec.repec.org/api/amf/RePEc:nbr:nberwo:{nber_id}'
                response = requests.get(url, timeout=self.timeout)
                status_code = response.status_code
                xml = et.fromstring(response.text)
                text = xml.getchildren()[0]
                reference = [x.getchildren()[0].text for x in text.getchildren() if 'isreferencedby' not in x.tag]
            except Exception:
                print(f'Exception happens to get references in WP {nber_id}. Don\'t worry, we\'ll try again.')
                sleep(self.sleep)
                continue
        
        try:
            return reference
        except UnboundLocalError:
            return None

    def get_acknowledgement(self, nber_id):
        url = f'https://www.nber.org/papers/w{nber_id}.ack'
        status_code = None
        while status_code != 200:
            try:
                ack = requests.get(url, timeout=self.timeout)
                status_code = ack.status_code
                content = BeautifulSoup(ack.content, features='html.parser')
                ack = content.find_all('div', {'align': 'left'})[0]
                ack = ack.get_text()
                ack = ack.split(' ')
                ack = ' '.join([x for x in ack if x.isalpha() == True])
            except Exception:
                print(f'Exception happens to get acknowledgements in WP {nber_id}. Don\'t worry, we\'ll try again.')
                sleep(self.sleep)
                continue

        return ack

    def get_paper(
        self,
        paper_id,
        citation_title,
        citation_author,
        citation_date,
        citation_publication_date,
        citation_technical_report_institution,
        citation_technical_report_number,
        citation_journal_title,
        citation_journal_issn,
        citation_pdf_url,
        topics,
        abstract,
        also_downloaded,
        total_cites,
        cited_by,
        reference,
        acknowledgement
    ):
        paper = {
            'id': paper_id,
            'citation_title': citation_title,
            'citation_author': citation_author,
            'citation_date': citation_date,
            'citation_publication_date': citation_publication_date,
            'citation_technical_report_institution': citation_technical_report_institution,
            'citation_technical_report_number': citation_technical_report_number,
            'citation_journal_title': citation_journal_title,
            'citation_journal_issn': citation_journal_issn,
            'citation_pdf_url': citation_pdf_url,
            'topics': topics,
            'abstract': abstract,
            'also_downloaded': also_downloaded,
            'total_cites': total_cites,
            'cited_by': cited_by,
            'reference': reference,
            'acknowledgement': acknowledgement
        }

        return paper

def main():
    nber = NBER()
    nber_id = nber.id
    latest_paper = nber.get_latest_paper()
    while nber_id <= latest_paper:
        url = f'https://www.nber.org/papers/w{nber_id}'
        print(f'Current paper: {url}')
        status_code = None
        while status_code != 200:
            try:
                response = requests.get(url, timeout=nber.timeout)
                status_code = response.status_code
            except Exception:
                print(f'Exception happens to get WP {nber_id}. Don\'t worry, we\'ll try again.')
                sleep(nber.sleep)
                continue
        content = BeautifulSoup(response.content, features='html.parser')
        paper = nber.get_paper(
            paper_id = nber_id,
            citation_title = nber.get_citation_item(content, 'citation_title'),
            citation_author = nber.get_citation_author(content),
            citation_date = nber.get_citation_item(content, 'citation_date'),
            citation_publication_date = nber.get_citation_item(content, 'citation_publication_date'),
            citation_technical_report_institution = nber.get_citation_item(content, 'citation_technical_report_institution'),
            citation_technical_report_number = nber.get_citation_item(content, 'citation_technical_report_number'),
            citation_journal_title = nber.get_citation_item(content, 'citation_journal_title'),
            citation_journal_issn = nber.get_citation_item(content, 'citation_journal_issn'),
            citation_pdf_url = nber.get_citation_item(content, 'citation_pdf_url'),
            topics = nber.get_topics(content),
            abstract = nber.get_abstract(content),
            also_downloaded = nber.get_also_downloaded(content),
            total_cites = nber.get_total_cites(nber_id),
            cited_by = nber.get_cited_by(nber_id),
            reference = nber.get_reference(nber_id),
            acknowledgement = nber.get_acknowledgement(nber_id)
        )
        df = pd.DataFrame([paper])
        file_name = './data/nber.csv'
        # if file already exists, it will check as follows:
        if os.path.exists(file_name):
            existing = pd.read_csv(file_name)
            # whether current ID already exists in the existing file or not
            if nber_id not in existing['id'].to_list():
                df = pd.concat([df, existing])
                df = df.sort_values(by='id', ascending=True)
                df.to_csv(file_name, index=False, header=True)
                print(f'{url} saved to CSV')
                nber_id += 1
                continue
            print(f'ID {nber_id} already exists, move to next ID')
            nber_id += 1
            continue
        # if file does not exist it will execute the following line
        df.to_csv(file_name, index=False, header=True)
        print(f'{url} saved to CSV')
        nber_id += 1

if __name__ == '__main__':
    intro = """
    Before we start, make sure you have good Internet connection because this
    program runs iteratively. For each working paper, there might be some
    connection issues. Between these iterations, this program will pause for 10
    seconds so that it doesn't burden the server (I try to scrape ethically :D).
    Consequently, it may take a long time to finish, especially if you run from
    the first to the latest working papers. So be patient :).\n
    """
    print(intro)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i',
        '--initial_id',
        type=int,
        default=1,
        help='NBER Working Paper ID to begin with (default is 1)',
        metavar=''
    )
    parser.add_argument(
        '-t',
        '--timeout',
        type=int,
        default=5,
        help='How long for each request before it timed out in seconds (default is 5)',
        metavar=''
    )
    parser.add_argument(
        '-s',
        '--sleep',
        type=int,
        default=10,
        help='How long to make time interval between iterations in case exception occurs in seconds (default is 10)',
        metavar=''
    )
    args = parser.parse_args()
    main()