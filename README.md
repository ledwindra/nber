![cron](https://github.com/ledwindra/nber/workflows/cron/badge.svg)

# About ✌🏽

Hello world :earth_asia:! Are you an economist, or economics student, or just a random person like me who is interested in economics? Do you want to write a paper, a thesis, or just ramble on some stuffs but don't have any fresh ideas on what should be the topic? Worry no more! Because, this repository is for you!

TL;DR: this repository contains a cron job that automatically scrapes NBER papers even if there's new paper coming and saves them. If you don't want to run this locally and just want to get straight to the data, [<strong>just chill, relax and, download here</strong>](https://media.githubusercontent.com/media/ledwindra/nber/master/data/nber.csv). 🌞 ⛱ 🥥 🌴 😎

If instead you want to try the scraper locally, you may want to consider this:

```
As of this writing, there are more than 20,000 working papers on NBER. If getting one paper takes around 30 seconds (including the required time interval imposed by NBER in its crawler policy, it takes more than a week to finish the program.
```

# Clone

If you think this is going to be useful for your purpose, don't hesitate to clone this repository:

```bash
git clone https://github.com/ledwindra/nber.git
cd nber
```

To get the latest updates, run `git pull origin master`

# Permission

Check its [<strong>`robots.txt`</strong>](http://data.nber.org/robots.txt)

Following is the snippet:

```
User-agent: *
Crawl-delay: 10

User-agent: *
Disallow: /fda/
Disallow: /contact/
Disallow: /confer/
Disallow: /~confer/
Disallow: /conf_papers/
Disallow: /c/
Disallow: /wpsubmit/
Disallow: /custom
Disallow: /confsubmit/
Disallow: /family/
Disallow: /1050/
Disallow: /cal/
Disallow: /cgi-bin/
Disallow: /nberhistory/historicalarchives/
Disallow: /xming*
Disallow: /taxex/
Disallow: /papers/mail
Disallow: /tmp/
Disallow: /server-status/
Disallow: /mrtg/
Disallow: /bb/
Disallow: /img/
Disallow: /pics
Disallow: /*.ris$
Disallow: /*.marc$
Disallow: /*.bib$
Disallow: /*palm*$
Disallow: /taxsim-calc*/
Disallow: /medicare/
Disallow: /*.pl/
Disallow: /arfpub/
Disallow: /pscp*
Disallow: /jobs/stateforms/
Disallow: /hcris/
```

Everybody is not disallowed to get `/papers/` tag. However, please scrape ethically by setting time interval between each request for 10 seconds (see `Crawl-delay: 10`).

# Virtual environment and install dependencies

Use virtual environment if you don't want to mess around with the existing modules installed on your machine. Just run the following on terminal:

```bash
python -m venv .venv-nber
source .venv-nber/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

To exit from virtual environment, type and press `deactivate`. 🌞 🌆

# Run
Before running the program, run the `python src/get_paper.py --help` and see the following message:

```
python src/get_paper.py --help

    Before we start, make sure you have good Internet connection because this
    program runs iteratively. For each working paper, there might be some
    connection issues. Between these iterations, this program will pause for 10
    seconds so that it doesn't burden the server (I try to scrape ethically :D).
    Consequently, it may take a long time to finish, especially if you run from
    the first to the latest working papers. So be patient :).

    
usage: get_paper.py [-h] [-i] [-t] [-s]

optional arguments:
  -h, --help          show this help message and exit
  -i , --initial_id   NBER Working Paper ID to begin with (default is 1)
  -t , --timeout      How long for each request before it timed out in seconds (default is 5)
  -s , --sleep        How long to make time interval between iterations in case exception occurs in seconds (default is 10)
```

This program takes three arguments to begin with. `initial_id`, if set as default, will scrape the NBER working papers from ID = 1 until the end. However, you can set this number, particularly if you don't want to redo the scraping because it will be redundant! Note that the observation will not be duplicated. Next, `timeout`, where it will wait until 5 seconds before the connection timed out. You can set the time longer if you want to, particularly when the internet connection is not really good. Lastly, `sleep` is the time interval between iterations if exception occurs (e.g. connection timeout error). According to NBER's crawler policy, the default time is 10 seconds. You can set it longer too. If all is clear, you can run the following program on terminal:

```
# without arguments
python src/get_paper.py

# with arguments
python src/get_paper.py --initial_id 1 --timeout 10 --sleep 15

# or shorter arguments
python src/get_paper.py -i 100 -t 15 -sleep 13
```

# Test

Run unit tests to ensure the data quality is good.

```
pytest test/test_paper.py
```

# Columns

|column_name|data_type|description|
|-|-|-|
|id|integer|A unique ID for each paper|
|citation_title|varchar|Paper title|
|citation_author|varchar|Paper author(s). Can be more than one. Hence it is stored as an array|
|citation_date|date|Date of paper (not clear)|
|citation_publication_date|date|Date of paper being published|
|citation_technical_report_institution|varchar|Institution which publishes the report|
|citation_technical_report_number|varchar|Paper ID as in the website|
|citation_journal_title|varchar|Journal title|
|citation_journal_issn|varchar|ISSN number|
|citation_pdf_url|varchar|Paper URL for PDF version|
|topics|varchar|Paper topic(s). Can be more tan one. Hence it is stored as an array|
|abstract|varchar|Paper abstract|
|also_downloaded|varchar|Users who downloaded this paper also downloaded* these. Can be more than one. Hence it is stored as an array. The reason I use the URL because otherwise it won't catch other than the working papers. Users may also download something from `/chapters/`, which may be useful for analysis|
|total_cites|integer|Total citations made by the author(s). Source: http://citec.repec.org/api/plain/RePEc:nbr:nberwo:[NBER_ID]|
|cited_by|integer|Total citations made by another author(s) from the author(s). Source: http://citec.repec.org/api/plain/RePEc:nbr:nberwo:[NBER_ID]|
|reference|varchar|A list of reference papers for each NBER paper. It is obtained from http://citec.repec.org/api/amf/RePEc:nbr:nberwo:[NBER_ID]
|acknowledgement|varchar|Paper's acknowledgement (in paragraph)|

# Contribute

If you happen to find bugs or issues when running the script, don't hesitate to submit the issues [<strong>`here`</strong>](https://github.com/ledwindra/nber/issues). If you'd like to contribute, here's what you can do:

1. Clone the repo
2. Create new branch by running `git checkout -b [YOUR-BRANCH-NAME]`
3. Always pull from `master` before push
4. Make `pull request`

The other way would be just contact me personally. I'm just a regular person.

# Closing

If you have read up to this line, thank you for bearing with me. Hope this is useful for your purpose! 😎 🍻
