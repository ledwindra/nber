import pytest
import pandas as pd
from random import randint

df = pd.read_csv('./data/nber.csv')
min_id = min(df['id'])
max_id = max(df['id'])
random_id = randint(min_id, max_id)
df = df[df['id'] == random_id]

class TestPaper:

    def test_id(self):
        global df
        assert (str(df.id) != '') == True
    
    def test_citation_title(self):
        global df
        assert (str(df.citation_title) != '') == True
    
    def test_citation_author(self):
        global df
        assert (str(df.citation_author) != '') == True

    def test_citation_date(self):
        global df
        assert (str(df.citation_date) != '') == True
    
    def test_citation_publication_date(self):
        global df
        assert (str(df.citation_publication_date) != '') == True
    
    def test_citation_technical_report_institution(self):
        global df
        assert (str(df.citation_technical_report_institution) != '') == True

    def test_citation_technical_report_number(self):
        global df
        assert (str(df.citation_technical_report_number) != '') == True
    
    def test_citation_journal_title(self):
        global df
        assert (str(df.citation_title) != '') == True
    
    def test_citation_journal_issn(self):
        global df
        assert (str(df.citation_journal_issn) != '') == True

    def test_citation_pdf_url(self):
        global df
        assert (str(df.citation_pdf_url) != '') == True
    
    def test_topics(self):
        global df
        assert (str(df.topics) != '') == True
    
    def test_abstract(self):
        global df
        assert (str(df.abstract) != '') == True
    
    def test_also_downloaded(self):
        global df
        assert (str(df.also_downloaded) != '') == True
    
    def test_total_cites(self):
        global df
        assert (str(df.total_cites) != '') == True

    def test_cited_by(self):
        global df
        assert (str(df.cited_by) != '') == True
    
    def test_reference(self):
        global df
        assert (str(df.reference) != '') == True
    
    def test_acknowledgement(self):
        global df
        assert (str(df.acknowledgement) != '') == True
